# This file is a template, and might need editing before it works on your project.
FROM httpd:alpine

COPY ./ /usr/local/apache2/htdocs/

docker build -t my-apache2 .

docker run -dit -p 8080:80 --name my-running-app my-apache2 
